# Resources For Beginners

##### Jason's Machine Learning 101
- slides by Jason Mayes (Senior Creative Engineer at Google)
  - https://plus.google.com/+JasonMayes/posts/dgMVgT5i543
  - https://docs.google.com/presentation/d/1kSuQyW5DTnkVaZEjGYCkfOxvzCqGEFzWBy4e9Uedd9k/edit#slide=id.g26e9e6b8ff_0_34

##### Stanford Seminar - "Deep Learning for Dummies" Carey Nachenberg of Symantec and UCLA CS
- https://www.youtube.com/watch?v=hvIptUuUCdU

https://www.youtube.com/playlist?list=PLZHQObOWTQDNU6R1_67000Dx_ZCJB-3pi

https://www.youtube.com/watch?v=R9OHn5ZF4Uo

https://www.youtube.com/watch?v=uXt8qF2Zzfo

https://www.youtube.com/watch?v=GvQwE2OhL8I

https://www.youtube.com/watch?v=yX8KuPZCAMo

https://www.youtube.com/watch?v=5DknTFbcGVM

https://www.youtube.com/watch?v=I74ymkoNTnw

https://www.youtube.com/watch?v=ZX2Hyu5WoFg

#### MIT Media Lab Publications 

Gender Shades: Intersectional Accuracy Disparities in Commercial Gender Classification 
- https://www.media.mit.edu/publications/gender-shades-intersectional-accuracy-disparities-in-commercial-gender-classification/
- Gives a brief overview of current gender biases in commerical face-classification algorithms. Specifically focuses on how to reduce bias given incomplete image sets.